(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("~/.zz_repo/documentos/Zz/Zettelkasten/org-roam/notas/20220922115533-ficha_de_leitura_empreendedorismo_unidade_1_de_uniceub.org" "/home/brasiliano/.zz_repo/documentos/Aa/Agenda/tarefas_e_compromissos/tarefas_e_compromissos.org")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:foreground "white" :background "red" :weight bold :height 2.5 :box (:line-width 10 :color "red"))))))
